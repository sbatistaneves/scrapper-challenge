import puppeteer from 'puppeteer';
import * as fs from 'fs';
import sanitizeHtml from 'sanitize-html';

const PAGE_URL = "https://www.hansimmo.be/appartement-te-koop-in-borgerhout/10161";

const main = async() => {
  
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  await page.goto(PAGE_URL);
  
  const items = await page.evaluate(() => {
    let description : string = document.querySelector('#description')!.innerHTML;
    let title : string = document.querySelector('#detail-title > .category')!.innerHTML;
    let price : string = document.querySelector('#detail-title > .price')!.innerHTML.replace(/&nbsp;/g, "");
    let address : string = document.querySelector('#detail-title > .address')!.innerHTML;
    return { description: description , title: title, price: price, address: address};
  });
  return items;
};

main().then((data) => {
  data.description = sanitizeHtml( data.description );
  fs.writeFile ("output.json", JSON.stringify(data), function(err) {
    if ( err ) throw err;
    console.info('json saved')
  });
});
